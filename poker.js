

let random = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min)
}


const numeros = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'A', 'J', 'Q', 'K']

const naipe = ['C', 'E', 'O', 'P']


const cartasMesa = []
let carta

while (cartasMesa.length < 5) {

    carta = numeros[random(0, 13)] + naipe[random(0, 4)]

    if (cartasMesa.includes(carta) == false) {
        cartasMesa.push(carta)
    }


}

let robo1 = []
let robo2 = []

function puxarCarta(play1, play2) {

    while (play1.length < 2) {
        carta = numeros[random(0, 13)] + naipe[random(0, 4)]

        if (cartasMesa.includes(carta) == false && play2.includes(carta) == false && play1.includes(carta) == false) {
            play1.push(carta)
        }
    }
    return play1
}


puxarCarta(robo1, robo2)
puxarCarta(robo2, robo1)





robo1 = robo1.concat(cartasMesa)
robo2 = robo2.concat(cartasMesa)



function verificaNaipe(array, naipe) {

    let n = array.filter((x) => {
        if (x[1] == naipe || x[2] == naipe) {
            return x
        }
    })
    return n
}


function verificaValor(array) {
    let valor = []
    for (let i = 0; i < array.length; i++) {
        valor.push(array[i][0])
    }
    return valor
}


function verificaValor_10_J_Q_K_A(robo) {

    let valor = verificaValor(robo)

    let index
    let sorted

    for (let i = 0; i < valor.length; i++) {
        if (valor[i] == '1') {
            index = valor.indexOf(valor[i])
            valor[index] = 10
        } else if (valor[i] == 'J') {
            index = valor.indexOf(valor[i])
            valor[index] = 11
        } else if (valor[i] == 'Q') {
            index = valor.indexOf(valor[i])
            valor[index] = 12
        } else if (valor[i] == 'K') {
            index = valor.indexOf(valor[i])
            valor[index] = 13
        } else if (valor[i] == 'A') {
            index = valor.indexOf(valor[i])
            valor[index] = 14
        }
    }

    let n = valor.map((x) => {
        return Number(x)
    })

    sorted = n.sort((a, b) => {
        return b - a
    })
    return sorted
}


function valorSeguidos(valores) { // 5 valores seguidos

    let cont = 0


    for (let i = 0; i < valores.length; i++) {


        if (valores[i] - valores[i + 1] == 1) {
            cont++
            if (cont >= 4) {
                return 5
            }
        }


    }


}

// -------------------------------------------------------------------------------------------------------------------------------------------


function royalFlush(robo) {  // funciona

    let c = verificaNaipe(robo, 'C')
    let e = verificaNaipe(robo, 'E')
    let o = verificaNaipe(robo, 'O')
    let p = verificaNaipe(robo, 'P')

    let valorC = verificaValor(c)
    let valorE = verificaValor(e)
    let valorO = verificaValor(o)
    let valorP = verificaValor(p)

    if (valorC.includes('1') && valorC.includes('J') && valorC.includes('Q') && valorC.includes('K') && valorC.includes('A')) {
        return `Royal flush `
    } else if (valorE.includes('1') && valorE.includes('J') && valorE.includes('Q') && valorE.includes('K') && valorE.includes('A')) {
        return 'Royal flush'
    } else if (valorO.includes('1') && valorO.includes('J') && valorO.includes('Q') && valorO.includes('K') && valorO.includes('A')) {
        return 'Royal flush'
    } else if (valorP.includes('1') && valorP.includes('J') && valorP.includes('Q') && valorP.includes('K') && valorP.includes('A')) {
        return 'Royal flush'
    }


}


function straightFlush(robo) { // funciona

    let c = verificaNaipe(robo, 'C')
    let e = verificaNaipe(robo, 'E')
    let o = verificaNaipe(robo, 'O')
    let p = verificaNaipe(robo, 'P')

    let valorC = verificaValor_10_J_Q_K_A(c)
    let valorE = verificaValor_10_J_Q_K_A(e)
    let valorO = verificaValor_10_J_Q_K_A(o)
    let valorP = verificaValor_10_J_Q_K_A(p)


    if (valorC.length >= 5 && valorC.includes(14) == false && valorSeguidos(valorC) == 5) {
        return 'Straight flush'
    } else if (valorE.length >= 5 && valorE.includes(14) == false && valorSeguidos(valorE) == 5) {
        return 'Straight flush'
    } else if (valorO.length >= 5 && valorO.includes(14) == false && valorSeguidos(valorO) == 5) {
        return 'Straight flush'
    } else if (valorP.length >= 5 && valorP.includes(14) == false && valorSeguidos(valorP) == 5) {
        return 'Straight flush'
    }




}


function quadra(robo) { // funciona 

    let a = verificaValor(robo)
    let array
    let q = false

    for (let i = 0; i < a.length; i++) {
        array = a.filter((x) => {
            return x == a[i]
        })

        if (array.length == 4) {
            q = true
        }

        if (array.length == 1 && q == true) {
            return 'Quadra'
        }
    }
}

function fullHouse(robo) { // FUNCIONA

    let a = verificaValor(robo)
    let array
    let full
    let house

    for (let i = 0; i < a.length; i++) {

        array = a.filter((x) => {
            return x == a[i]
        })

        if (array.length >= 3) {
            full = 'Full'

        } else if (array.length >= 2) {
            house = 'house'

        }

    }
    if (full + ' ' + house == 'Full house') {
        return 'Full house'
    }
}



function flush(robo) { // funciona

    let c = verificaNaipe(robo, 'C')
    let e = verificaNaipe(robo, 'E')
    let o = verificaNaipe(robo, 'O')
    let p = verificaNaipe(robo, 'P')


    let valorC = verificaValor_10_J_Q_K_A(c)
    let valorE = verificaValor_10_J_Q_K_A(e)
    let valorO = verificaValor_10_J_Q_K_A(o)
    let valorP = verificaValor_10_J_Q_K_A(p)


    if (valorC.length >= 5 && valorSeguidos(valorC) != 5) {
        return 'Flush'
    } else if (valorE.length >= 5 && valorSeguidos(valorE) != 5) {
        return 'Flush'
    } else if (valorO.length >= 5 && valorSeguidos(valorO) != 5) {
        return 'Flush'
    } else if (valorP.length >= 5 && valorSeguidos(valorP) != 5) {
        return 'Flush'
    }





}



function straight(robo) { // funciona


    let valor = verificaValor(robo)
    let novoArray = []
    let index
    let sorted
    let cont = 0
    for (let i = 0; i < valor.length; i++) {
        if (valor[i] == '1') {
            index = valor.indexOf(valor[i])
            valor[index] = 10
        } else if (valor[i] == 'J') {
            index = valor.indexOf(valor[i])
            valor[index] = 11
        } else if (valor[i] == 'Q') {
            index = valor.indexOf(valor[i])
            valor[index] = 12
        } else if (valor[i] == 'K') {
            index = valor.indexOf(valor[i])
            valor[index] = 13
        } else if (valor[i] == 'A') {
            index = valor.indexOf(valor[i])
            valor[index] = 14
        }
    }

    let n = valor.map((x) => {
        return Number(x)
    })

    for (let x = 0; x < n.length; x++) {

        if (robo[x][1] == '0') {
            novoArray.push([n[x], robo[x][2]])
        } else {
            novoArray.push([n[x], robo[x][1]])
        }
    }



    sorted = novoArray.sort((a, b) => {
        return b[0] - a[0]
    })

    /*
  
    else if (cont == 3 && sorted[sorted.length - 2][0] - sorted[sorted.length - 1][0] == 1) {
                  cont++
              }
  
    */


    for (let x = 0; x < sorted.length; x++) {

        if (x < 6) {

            if (sorted[x][0] - sorted[x + 1][0] == 1 && sorted[x][1] != sorted[x + 1][1]) {
                cont++
            } else {
                if (cont > 0) {
                    cont--
                }
            }

        } else if (cont == 3 && sorted[sorted.length - 2][0] - sorted[sorted.length - 1][0] == 1) {
            cont++
        }

        if (cont == 4) {
            return 'Straight'
        }

    }







}







function trio(robo) { // funciona


    let a = verificaValor(robo)

    let array
    let arrayTrio = []
    let cont = 0
    for (let i = 0; i < a.length; i++) {

        array = a.filter((x) => {
            return x == a[i]
        })

        if (array.length == 1) {
            cont++
        }

        if (array.length == 3 && arrayTrio.includes(a[i]) == false) {
            arrayTrio.push(a[i])
        }

        if (cont >= 2 && arrayTrio.length == 1) {
            return 'Trio'
        }

    }

}


function doisPares(robo) { // funciona


    let a = verificaValor(robo)

    let array
    let arrayPares = []
    for (let i = 0; i < a.length; i++) {

        array = a.filter((x) => {
            return x == a[i]
        })


        if (array.length == 2 && arrayPares.includes(a[i]) == false) {
            arrayPares.push(a[i])

        }

        if (arrayPares.length == 2 && array.length == 1) {
            return 'Dois pares'
        }

    }



}


function par(robo) { // funciona

    let a = verificaValor(robo)

    let array

    for (let i = 0; i < a.length; i++) {

        array = a.filter((x) => {
            return x == a[i]
        })


        if (array.length == 2) {
            return 'Par'
        }

    }



}


function cartaAlta(robo) { // funciona

    let maior = 0

    let n = verificaValor_10_J_Q_K_A(robo)

    for (let i = 0; i < n.length; i++) {
        if (n[i] > maior) {
            maior = n[i]
        }
    }
    return maior
}

function resultado(robo) {

    if (royalFlush(robo) != undefined) {
        return royalFlush(robo)

    } else if (straightFlush(robo) != undefined) {
        return straightFlush(robo)

    } else if (quadra(robo) != undefined) {
        return quadra(robo)

    } else if (fullHouse(robo) != undefined) {
        return fullHouse(robo)

    } else if (flush(robo) != undefined) {
        return flush(robo)

    } else if (straight(robo) != undefined) {
        return straight(robo)

    } else if (trio(robo) != undefined) {
        return trio(robo)

    } else if (doisPares(robo) != undefined) {
        return doisPares(robo)

    } else if (par(robo) != undefined) {
        return par(robo)

    } else if (cartaAlta(robo) != undefined) {
        return 'Carta alta'
    }

}



function valorMao(robo) {

    if (royalFlush(robo) != undefined) {
        return 10

    } else if (straightFlush(robo) != undefined) {
        return 9

    } else if (quadra(robo) != undefined) {
        return 8

    } else if (fullHouse(robo) != undefined) {
        return 7

    } else if (flush(robo) != undefined) {
        return 6

    } else if (straight(robo) != undefined) {
        return 5

    } else if (trio(robo) != undefined) {
        return 4

    } else if (doisPares(robo) != undefined) {
        return 3

    } else if (par(robo) != undefined) {
        return 2

    } else if (cartaAlta(robo) != undefined) {
        return 1
    }

}



let p1 = valorMao(robo1)
let p2 = valorMao(robo2)


function jogo(valor1, valor2) {

    let r1 = resultado(robo1)
    let r2 = resultado(robo2)



    if (valor1 > valor2) {
        return `Robô 1: ${r1} (${robo1}), Robô 2: ${r2} (${robo2}), Robô 1 venceu!`
    } else if (valor1 < valor2) {
        return `Robô 2: ${r2} (${robo2}), Robô 1: ${r1} (${robo1}), Robô 2 venceu!`
    } else {
        return `Robô 1: ${r1} (${robo1}), Robô 2: ${r2} (${robo2}), empate!`
    }

}


let j = jogo(p1, p2)
document.write(j)




/*-----------------------//testes//--------------------------------------------------

let t1 = ['AC', 'JC', 'KC', 'QC', '10C'] // royal flush
console.log(royalFlush(t1))
let t2 = ['2E', '3E', '4E', '5E', '6E'] // straight flush
console.log(straightFlush(t2))
let t3 = ['2E', '2C', '2O', '2P', '5C'] // quadra
console.log(quadra(t3))
let t4 = ['2E', '2C', '2O', '8C', '8E'] //full house
console.log(fullHouse(t4))
let t5 = ['2O', '5O', '6O', '3O', '7O'] // flush
console.log(flush(t5))
let t6 = ['2C', '3O', '4C', '5O', '6C'] // straight 
console.log(straight(t6))
let t7 = ['3C', '3E', '3O', '5P', '7O'] // trio
console.log(trio(t7))
let t8 = ['2C', '2O', '4P', '4E', '7C'] // dois pares
console.log(doisPares(t8))
let t9 = ['3E', '3O', '3C', '4E', '4O'] // par
console.log(par(t9))
let t10 = ['10C', '5C', '2C', '2O', '6E'] // carta alta
console.log(cartaAlta(t10))


-----------------------------------------------------------------------------------------------------

*/
