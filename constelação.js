

let numeroAleatorio = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min)
}

let entrada = {
    qtd: Number(prompt('quantidade de estrelas:')),
    first: Number(prompt('primeira estrela:')),
    sec: Number(prompt('segunda estrela:'))
}


while (entrada.qtd < 4 || entrada.qtd > 8) {
    alert('escolha entre 4 e 8, para quantidade de estrelas ')
    entrada.qtd = Number(prompt('quantidade de estrelas:'))
}

const constelacao = new Array(entrada.qtd)
let estrelas = []

for (let i = 0; i < constelacao.length; i++) {

    for (let x = 0; x < entrada.qtd; x++) {
        estrelas[x] = numeroAleatorio(0, 2)
    }
    constelacao[i] = estrelas
    estrelas = []
}

for (let i = 0; i < constelacao.length; i++) {
    document.write(constelacao[i] + '<br>')
}

const priEstre = constelacao[entrada.first]
const segEstre = constelacao[entrada.sec]
let lig = priEstre.length
for (let x = 0; x < priEstre.length; x++) {
    if (priEstre[x] != segEstre[x]) {
        lig--
    }
}
if (lig == 0) {
    lig = "há ligação"
} else {
    lig = "não há ligação"
}
document.write(lig)