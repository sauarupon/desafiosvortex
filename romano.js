
// O número pode ser no intervalo de I à MMMMCMXCIX : 4999

let numeroRomano = prompt('número romano:').toUpperCase()
let arrayRomano = numeroRomano.split('')

let erro = arrayRomano.every((x) => {
    return x == "X" || x == 'V' || x == 'I' || x == 'L' || x == 'D' || x == 'M' || x == 'C'
})

while (erro == false) {
    alert('número romano inválido')
    numeroRomano = prompt('numero romano:').toUpperCase()
    arrayRomano = numeroRomano.split('')
    erro = arrayRomano.every((x) => {
        return x == "X" || x == 'V' || x == 'I' || x == 'L' || x == 'D' || x == 'M' || x == 'C'
    })
}



let atual
let ant
let prox
let acc = 0

for (let i = 0; i < arrayRomano.length; i++) {
    ant = arrayRomano[i - 1]
    atual = arrayRomano[i]
    prox = arrayRomano[i + 1]

    if (atual == 'I' && arrayRomano[i + 1] == 'I' && arrayRomano[i + 2] == 'I' && arrayRomano[i + 3] == 'I' && arrayRomano[i + 4] == 'I') {
        acc = 'número romano inválido'
        break
    } else if (atual == 'X' && arrayRomano[i + 1] == 'X' && arrayRomano[i + 2] == 'X' && arrayRomano[i + 3] == 'X' && arrayRomano[i + 4] == 'X') {
        acc = 'número romano inválido'
        break
    } else if (atual == 'C' && arrayRomano[i + 1] == 'C' && arrayRomano[i + 2] == 'C' && arrayRomano[i + 3] == 'C' && arrayRomano[i + 4] == 'C') {
        acc = 'número romano inválido'
        break
    } else if (atual == 'M' && arrayRomano[i + 1] == 'M' && arrayRomano[i + 2] == 'M' && arrayRomano[i + 3] == 'M' && arrayRomano[i + 4] == 'M') {
        acc = 'número romano inválido'
        break
    }



    if (atual + prox == 'DD' || atual + prox == 'LL' || atual + prox == 'VV') {
        acc = 'número romano inválido'
        break
    }



    if (atual == 'I') {

        if (prox == 'C' || prox == 'D' || prox == 'M' || prox == 'L') {
            acc = 'número romano inválido'
            break
        } else {
            acc += 1
        }
    } else if (atual == 'M') {

        if (ant == 'C') {

            if (prox == 'C' || prox == 'D' || prox == 'M') {
                acc = 'número romano inválido'
                break
            } else {
                if (arrayRomano[i - 2] == 'M') {
                    acc += 800
                } else {
                    acc += 900
                }

            }

        } else {
            acc += 1000
        }

    } else if (atual == 'D') {
        if (prox == 'M') {
            acc = 'número romano inválido'
            break
        } else if (ant == 'C') {
            if (prox == 'C' || prox == 'D' || prox == 'M') {
                acc = 'número romano inválido'
                break
            } else {
                if (arrayRomano[i - 2] == 'M') {
                    acc += 300
                } else {
                    acc += 400
                }
            }
        } else {
            acc += 500
        }
    } else if (atual == 'C') {
        if (ant == 'X') {
            if (prox == 'M' || prox == 'D' || prox == 'C' || prox == 'L' || prox == 'X') {
                acc = 'número romano inválido'
                break
            } else {
                acc += 80
            }
        } else {
            acc += 100
        }
    } else if (atual == 'L') {
        if (prox == 'C' || prox == 'D' || prox == 'M') {
            acc = 'número romano inválido'
            break
        } else if (ant == 'X') {
            if (prox == 'M' || prox == 'D' || prox == 'C' || prox == 'L' || prox == 'X') {
                acc = 'número romano inválido'
                break
            } else {
                acc += 30
            }
        } else {
            acc += 50
        }
    } else if (atual == 'X') {
        if (prox == 'D' || prox == 'M') {
            acc = 'número romano inválido'
            break
        } else if (ant == 'I') {
            if (prox == 'M' || prox == 'D' || prox == 'C' || prox == 'L' || prox == 'X' || prox == 'I') {
                acc = 'número romano inválido'
                break
            } else {
                acc += 8
            }
        } else {
            acc += 10
        }
    } else if (atual == 'V') {
        if (ant == 'I' && arrayRomano[i - 2] == 'I') {
            acc = 'número romano inválido'
            break
        } else if (prox != 'I' && prox != undefined) {
            acc = 'número romano inválido'
            break
        } else if (ant == 'I') {
            if (prox == 'M' || prox == 'D' || prox == 'C' || prox == 'L' || prox == 'X' || prox == 'I') {
                acc = 'número romano inválido'
                break
            } else {
                acc += 3
            }
        } else {
            acc += 5
        }
    }




}
document.write(numeroRomano + '<br>')
document.write(acc)


